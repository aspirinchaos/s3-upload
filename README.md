# S3Upload

Пакет для загрузки файлов с сервера на Spaces S3.

Пакет хорошо работает совместно с пакетом [tomi:upload-server](https://github.com/tomitrescak/meteor-uploads).

## Настройка

Для работы загрузки необходимо передать в инстанс ключи доступа к Space.
```javascript
import { S3Upload } from 'meteor/s3-upload';

S3Upload.setSettings({
  accessKeyId, // ид ключа S3
  secretAccessKey, // ключ S3
  Bucket, // имя ведра 
  endpoint, // путь доступа к ведру 
  uploadDir, // директория из которой будут загружаться файлы
});

```

## Загрузка файлов

Загрузка файлов происходит через вызов метода `uploadFile` инстанса `S3Upload`.

В метод передаются два параметра:
- `filename` {string} - имя/путь файла
- `bucketPath` {string} - путь сохранения в Space

Метод возвращает `Promise`, который вернет путь к файлу на Space&

```javascript
import { S3Upload } from 'meteor/s3-upload';

S3Upload.uploadFile(filename, bucketPath);

```
