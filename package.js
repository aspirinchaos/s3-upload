Package.describe({
  name: 's3-upload',
  version: '0.1.3',
  summary: 'Upload image to 3s',
  git: 'https://bitbucket.org/aspirinchaos/s3-upload.git',
  documentation: 'README.md',
});


Npm.depends({
  'aws-sdk': '2.293.0',
  mime: '2.4.4',
});


Package.onUse((api) => {
  api.versionsFrom('1.5');
  api.use('ecmascript');
  api.mainModule('s3-upload.js', 'server');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('s3-upload');
  api.mainModule('s3-upload-tests.js');
});
