import AWS from 'aws-sdk';
import fs from 'fs';
import mime from 'mime';

/**
 * @module meteor/s3-upload
 */

class S3Upload {
  /**
   * инстанс для работы с DO Space
   * @type {AWS.S3}
   */
  _s3 = null;

  _logError(error) {
    console.log('==========S3-Upload-Error==========');
    console.log(error);
    console.log('==========S3-Upload-Error==========');
  }

  /**
   * Установка настроек для DO Space
   * @param accessKeyId {string} - ид ключа
   * @param secretAccessKey {string} - секрет ключа
   * @param Bucket {string} - имя ведра
   * @param endpoint {string} - путь доступа ведра
   */
  setSettings({ accessKeyId, secretAccessKey, Bucket, endpoint }) {
    if (this._s3) {
      console.error('S3Upload уже настроен!');
      return;
    }
    AWS.config.update({ accessKeyId, secretAccessKey });
    this._s3 = new AWS.S3({
      endpoint: new AWS.Endpoint(endpoint),
      params: { Bucket },
    });
    this._s3.listBuckets().promise().then((data) => {
      if (!data.Buckets.map(item => item.Name).some(name => name === Bucket)) {
        console.log('Неверное имя space!');
      }
    }).catch(this._logError);
  }

  /**
   * Промификация считывания файла
   * @param filename {string} - имя файла
   * @return {Promise<Buffer>}
   */
  _readFile(filename) {
    return new Promise((resolve, reject) => {
      fs.readFile(filename, (err, content) => {
        if (err) {
          reject(err);
        }
        resolve(content);
      });
    });
  }

  /**
   * Удаления файла после загрузки
   * @param filename {string} - имя файла
   */
  _deleteFile(filename) {
    fs.unlink(filename, (err) => {
      if (err) {
        console.log(err);
      }
    });
  }

  /**
   * Загрузка файла в Space
   * @param Key {string} - путь для хранения в Space
   * @param Body {Buffer} - содержимое файла
   * @param ContentType {string} - содержимое файла
   * @return {Promise<Object>}
   */
  _uploadS3(Key, Body, ContentType) {
    return this._s3.upload({ Key, Body, ContentType, ACL: 'public-read' }).promise();
  }

  /**
   * Загрузка файла в Space
   * @param filename {string} - имя файла
   * @param loadpath {string} - путь для хранения в Space
   * @return {Promise<string>} - путь к файлу в Space
   */
  uploadFile(filename, loadpath) {
    const type = mime.getType(filename);
    return this._readFile(filename)
      .then(content => this._uploadS3(loadpath, content, type)).then(({ Location }) => {
        this._deleteFile(filename);
        return Location;
      }).catch(this._logError);
  }

  /**
   * Удаление файла в Space
   * @param Key {string} - путь для хранения в Space
   * @return {Promise<Object>}
   */
  deleteFile(Key) {
    return this._s3.deleteObject({ Key }).promise().catch(this._logError);
  }
}

const S3U = new S3Upload();

export { S3U as S3Upload };
